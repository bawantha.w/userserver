const router = require('express').Router();
const User = require('../model/user');
const JWT = require('jsonwebtoken');
const { secretCode } = require('../appConfig');
const { passwordEncryptor, passwordDecryptor } = require('../services/securePassword');
const {
  registerValidation,
  loginValidation,
  updateValidation
} = require('../services/userValidation');
const { emailFetcher } = require('../services/tokenValidation');
const { decodeImage } = require('../services/imageConverter');
// const { collection } = require('../appConfig');

// registration router
/**
 * @swagger
 * /api/user/register:
 *  post:
 *    tags:
 *      - User
 *    description: Authenticate user to register in the system
 *    parameters:
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/userRegisterRequest'
 *    responses:
 *      200:
 *        description: An access token that contains user email
 * definitions:
 *   userRegisterRequest:
 *     properties:
 *       userName:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *       address:
 *         type: string
 */
router.post('/register', async (req, res) => {
  // validate data using Joi
  const { error } = registerValidation(req.body);
  if (error) return res.status(400).send('Invalid Input');

  // checking the existence of the email
  const emailExist = await User.findOne({ email: req.body.email });
  if (emailExist) return res.status(400).send('Email Already In Use');

  // encrypt password using crypto
  const password = req.body.password;
  const newPassword = passwordEncryptor(password);

  // decode base64 string and saving image file in assets/images folder
  const imageFile = req.body.image;
  let nameTemplate = null;
  if (imageFile) {
    const name = req.body.userName;
    nameTemplate = `${name}-decoded-image.jpg`;
    decodeImage(imageFile, nameTemplate);
  }

  // save a new user
  const user = new User({
    userName: req.body.userName,
    email: req.body.email,
    password: newPassword,
    address: req.body.address,
    image: nameTemplate
  });

  // Saving process validation
  try {
    await user.save();
    // create a token and pass that token as the response
    const token = JWT.sign({ _id: user._id, email: user.email }, secretCode);
    res.status(200).send(token);
  } catch (error) {
    res.status(400).send(error);
  }
});

// login
/**
 * @swagger
 * /api/user/login:
 *  post:
 *    tags:
 *      - User
 *    description: Authenticate user to login the system
 *    parameters:
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/userLoginRequest'
 *    responses:
 *      200:
 *        description: An access token that contains user email
 * definitions:
 *   userLoginRequest:
 *     properties:
 *       email:
 *         type: string
 *       password:
 *         type: string
 */
router.post('/login', async (req, res) => {
  const { error } = loginValidation(req.body);
  if (error) return res.status(400).send('Invalid Input');

  const user = await User.findOne({ email: req.body.email });
  if (!user) return res.status(404).send('Email Not In Use');

  const enteredPassword = req.body.password;
  const storedPassword = user.password;
  // decrypt stored password
  const decryptedStoredPassword = passwordDecryptor(storedPassword);

  // compare entered password with password stored in the database
  if (enteredPassword === decryptedStoredPassword) {
    // create a token and pass that token as the response
    const token = JWT.sign({ _id: user._id, email: user.email }, secretCode);
    res.status(200).send(token);
  } else {
    res.send('Check Your Password');
  }
});

// update
/**
 * @swagger
 * /api/user/updateName:
 *  put:
 *    tags:
 *      - User
 *    description: Allow user to update the system
 *    parameters:
 *       - name: 'Authorization'
 *         description: User object
 *         in: header
 *         required: true
 *         schema:
 *           $ref: '#/definitions/requestHeader'
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/userUpdateRequest'
 *    responses:
 *      200:
 *        description: An access token that contains user email
 * definitions:
 *   requestHeader:
 *     properties:
 *       Authorization:
 *         type: string
 *   userUpdateRequest:
 *     properties:
 *       userName:
 *         type: string
 */
router.put('/updateName', async (req, res) => {
  const fetchedEmail = emailFetcher(req);
  const userDetails = await User.findOne({ email: fetchedEmail });
  await userDetails.updateOne({
    userName: req.body.userName
  });
  const validatedName = updateValidation(req.body.userName);
  if (!validatedName) res.status(400).send('Invalid name use entered to the updated name');
  res.status(200).send('Updated the name of the user');
});

module.exports = router;
