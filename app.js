const express = require('express');
const app = express();
const mongoose = require('mongoose');
const { db_url, port } = require('./appConfig');
const swaggerJsDocs = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

// Connect server to mongodb database
mongoose.connect(db_url, { useNewUrlParser: true, useUnifiedTopology: true }).then(() => {
  console.log('Connected to the mongodb database...');
});

// Importing routes
const authUserRoute = require('./routes/authUser');

// middleware
app.use(express.json({ limit: '50mb' }));
// app.use(bodyParser.json());

// Route middlewares
app.use('/api/user', authUserRoute);

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'User Server',
      description: 'Server Side API For Users',
      contact: {
        name: 'Bawantha'
      },
      server: ['http://localhost:9000']
    }
  },
  apis: ['./routes/authUser.js']
};

const swaggerDocs = swaggerJsDocs(swaggerOptions);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.listen(port, () => {
  console.log('Server started successfully...');
});
