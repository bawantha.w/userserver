const JWT = require('jsonwebtoken');
const { secretCode } = require('../appConfig');

const emailFetcher = (req) => {
  try {
    const token = req.header('Authorization').split(' ')[0];

    if (token == null) return 'Not A Valid Token';
    const decoded = JWT.verify(token, secretCode);
    const userEmail = decoded.email;
    return userEmail;
  } catch (error) {
    return 'Invalid';
  }
};

module.exports = { emailFetcher };
