const image2base64 = require('image-to-base64');
const fs = require('fs');
const path = require('path');

// app.use(bodyParser.json({ limit: '50mb' }));
// app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));

// const encodeImage = async (file, fileName) => {
//   try {
//     await image2base64(file).then((encodedText) => {
//       const localName = fileName;
//       fs.writeFileSync(localName, encodedText);
//     });
//     const currentPath = path.join('./', fileName);
//     const newPath = path.join('./assets', 'encodedImages', fileName);

//     fs.rename(currentPath, newPath, function (err) {
//       if (err) {
//         throw err;
//       } else {
//         console.log('Successfully moved the file!');
//       }
//     });
//     return true;
//   } catch (error) {
//     return false;
//   }
// };

const decodeImage = async (file, fileName) => {
  try {
    const decodedFile = await new Buffer.from(file, 'base64');
    fs.writeFileSync(fileName, decodedFile);

    const currentPath = path.join('./', fileName);
    const newPath = path.join('./assets', 'images', fileName);

    fs.rename(currentPath, newPath, function (err) {
      if (err) {
        throw err;
      } else {
        console.log('File moved to assests/images folder...');
      }
    });
    return true;
  } catch (error) {
    return false;
  }
};

// app.get('/', async (req, res) => {
//   const data = req.body.text;
//   let buff = await new Buffer.from(data, 'base64');
//   const pick = 'canva2';
//   const template = `${pick}-encode.jpg`;
//   //   console.log('Template-----------------------------------------: ' + template);
//   fs.writeFileSync(template, buff);
//   res.send(buff);
// });

module.exports = { decodeImage };
