const { key } = require('../appConfig');
const crypto = require('crypto');

const passwordEncryptor = (password) => {
  const encryptedPassword = crypto.createCipher('aes-256-ctr', key).update(password, 'utf8', 'hex');
  return encryptedPassword;
};

const passwordDecryptor = (password) => {
  const storedPassword = password;
  const decryptedStoredPassword = crypto
    .createDecipher('aes-256-ctr', key)
    .update(storedPassword, 'hex', 'utf8');
  return decryptedStoredPassword;
};

module.exports = { passwordEncryptor, passwordDecryptor };
